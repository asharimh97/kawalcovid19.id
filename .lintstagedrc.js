// From next.js' lint-staged
// https://github.com/zeit/next.js/blob/canary/lint-staged.config.js
// https://github.com/okonet/lint-staged/issues/676#issuecomment-593390071

const escape = require('shell-quote').quote;
const isWin = process.platform === 'win32';

module.exports = {
  '**/*.{js,jsx,ts,tsx,json,css,scss,html,yml,yaml}': filenames => {
    const escapedFileNames = filenames
      .map(filename => `"${isWin ? filename.replace(/\[|\]/g, '[$&]') : escape([filename])}"`)
      .join(' ');
    return [`prettier --write ${escapedFileNames}`, `git add ${escapedFileNames}`];
  },
};
