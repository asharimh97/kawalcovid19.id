import * as React from 'react';
import styled from '@emotion/styled';
import { themeProps, Heading, Paragraph } from 'components/design-system';
import { Column, Content } from 'components/layout';

const Section = Content.withComponent('section');

const Root = styled(Section)`
  padding: 6vh 24px;

  ${themeProps.mediaQueries.lg} {
    padding: 12vh 24px;
  }
`;

const Hero: React.FC = () => {
  return (
    <Root noPadding>
      <Column>
        <Heading variant={900} mb="md" maxWidth={800} as="h1">
          Kawal informasi seputar COVID-19 secara tepat dan akurat.
        </Heading>
        <Paragraph variant={500} maxWidth={800}>
          Situs ini merupakan sumber informasi inisiatif sukarela warganet Indonesia pro-data,
          terdiri dari praktisi kesehatan, akademisi & profesional.
        </Paragraph>
      </Column>
    </Root>
  );
};

export default Hero;
