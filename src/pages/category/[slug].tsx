import * as React from 'react';
import { NextPage, GetStaticPaths, GetStaticProps } from 'next';

import { wp } from 'utils/api';
import { WordPressPostIndex, WordPressCategory, WordPressUser } from 'types/wp';
import { PageWrapper, Content, Column } from 'components/layout';
import { Box, Text } from 'components/design-system';
import { SectionHeader, PostIndexCard } from 'modules/posts-index';
import { ArticlesListGrid } from 'modules/home';
import getAuthorsDetail from 'utils/wp/getAuthorsDetail';

interface IndexPageProps {
  category?: WordPressCategory;
  posts?: WordPressPostIndex[];
  authors?: Record<string, WordPressUser>;
  errors?: string;
}

const Section = Content.withComponent('section');

const CategoryIndexPage: NextPage<IndexPageProps> = ({ category, posts, authors }) => {
  const categorySlugWithExcerpt = ['informasi', 'panduan', 'bacaan'];

  return (
    <PageWrapper pageTitle="Informasi">
      {category && <SectionHeader title={category.name} description={category.description} />}
      <Section>
        <Column>
          {posts ? (
            <Box mb="xxl">
              <ArticlesListGrid>
                {posts.map(post => (
                  <PostIndexCard
                    key={post.slug}
                    post={post}
                    author={authors?.[post.author.toString()]}
                    hasExcerpt={categorySlugWithExcerpt.includes(category?.slug || '')}
                  />
                ))}
              </ArticlesListGrid>
            </Box>
          ) : (
            <Box my="xxl" textAlign="center">
              <Text color="accents06">Tidak ada konten ditemukan.</Text>
            </Box>
          )}
        </Column>
      </Section>
    </PageWrapper>
  );
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  try {
    if (params) {
      const { slug } = params;
      const category = await wp('wp/v2/categories', {
        slug,
        _fields: 'id,count,description,name,slug',
      });

      if (category && category[0]) {
        const unfilteredPosts = await wp<WordPressPostIndex[]>('wp/v2/posts', {
          categories: category[0].id,
          _fields: 'id,date_gmt,modified_gmt,type,slug,title,excerpt,author',
          per_page: 100,
        });

        if (Array.isArray(unfilteredPosts)) {
          const posts = unfilteredPosts.filter(post => post.type === 'post');

          if (posts?.length) {
            const authorsIdList = posts.map(post => post.author);

            const uniqueAuthors = [...new Set(authorsIdList)];
            const authorsDetail = await getAuthorsDetail(uniqueAuthors);
            const map = authorsDetail.reduce<Record<string, WordPressUser>>((obj, item) => {
              // eslint-disable-next-line no-param-reassign
              obj[item.id] = item;
              return obj;
            }, {});

            const authors = map;

            return { props: { category: category[0], posts, authors } };
          }

          return { props: { category: category[0], posts } };
        }

        return { props: { category: category[0], posts: [] } };
      }
    }

    throw new Error('Failed to fetch category');
  } catch (err) {
    return { props: { errors: err.message } };
  }
};

export const getStaticPaths: GetStaticPaths = async () => {
  const pagesList = await wp<WordPressPostIndex[]>('wp/v2/categories', {
    per_page: 100,
  });

  const paths =
    pagesList && pagesList.length
      ? pagesList.map(page => {
          return { params: { slug: page.slug } };
        })
      : [];

  return {
    fallback: true,
    paths,
  };
};

export default CategoryIndexPage;
